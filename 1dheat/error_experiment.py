import subprocess as sp 
import os
import numpy as np 
#import matplotlib.pyplot as plt 
import json

"""
Compile using included makefile
name - name of the makefile stage to run. Ex: "cuda_imr"
nx - the number of points in the grid to be run

no return

Note: I did try using subprocess but make wouldn't use the variables.
so I just used os.system
"""
def compile(name, nx):
        cmd = f"make {name} nx={nx}"
        #print(cmd)
        #print(cmd.split())
        #sp.run(['make', f"full_prec='{d_full}'", f"redu_prec='{d_redu}'"], shell=True, check=True)
        #sp.run(cmd.split(), shell=True, check=True)
        stat = os.system(cmd)
        if stat != 0:
                print(f"ERROR: exit status {stat}")

"""
Run the program
nt - Program argument for nt, ie the number of timesteps

program outputs the time to run the method, so capture it and return
"""
def run_prog(nt, fname, n_runs=5):
        #dt computation. This will be the dt used within the run. Just return it
        ti = 0.0
        tf = 0.1
        timesteps = np.linspace(ti, tf, nt, endpoint=False)
        dt = timesteps[1] - timesteps[0]
        #on to the runs
        t = 0
        e = 0
        for i in range(n_runs):
                out = sp.run([f'./{fname}', f'{nt}'], capture_output=True)
                #print(out.stdout.split())
                lines = out.stdout.splitlines()
                for line in lines:
                        ls = str(line).split()
                        #print(ls)
                        if "method" in ls:
                            t = t + float(ls[-1][:-1])
                        if "Error" in ls:
                                e = e + float(ls[-1][:-1])
        return t/n_runs, e/n_runs, dt

"""
Run the full experiment for one of the tests
test_name - the name of the test to run. Name must match the directory 
and teh name of the test.

Writes the results of the experiment inside the test directory to a 
json file
"""
def experiment(test_name, nx):
        workdir = os.path.join('./', test_name)
        if not os.path.exists(workdir):
                print(f"experiment directory {test_name} doesn't exist. Returning...")
        os.chdir(workdir)

        fnames = [f'{test_name}-dd.x', f'{test_name}-ds.x', f'{test_name}-ss.x', f'{test_name}-1p.x', f'{test_name}-2p.x']
        min_nt = 300
        nts = list(map(lambda p: min_nt * 2**p, range(8)))
        
        runs = []
        compile('imr', nx)
        for fname in fnames:
                run = {'fname': fname, 'errs': [], 'ts': [], 'dts': []}
                for nt in nts:
                        print(f"Running {fname} for nt={nt}")
                        t, e, dt = run_prog(nt, fname)
                        print(f'ave_runtime={t}, error={e}, dt={dt}')
                        run['dts'].append(dt)
                        run['errs'].append(e)
                        run['ts'].append(t)
                runs.append(run)

        with open(f'runs_{test_name}_{nx}.json', 'w') as f:
                json.dump(runs, f)

def main():
        #run the reference solution
        nx = 200
        os.chdir('./ref_sol')
        compile('', nx)
        ref_nt = 10**7
        print(f"Running reference for {ref_nt}")
        out = sp.run([f'./rk4.x', f'{ref_nt}', '1'], capture_output=True)
        os.chdir('./..')
        #Use multiprocess to run all tests at once. 
        #Theoretically this shouldn't impact timing results because the 
        #tests are all serial. Requires >=4 hardware threads for this theory
        #to work
        tests = ['impmid']
        for test in tests:
                experiment(test, nx)

if __name__ == '__main__':
        main()
