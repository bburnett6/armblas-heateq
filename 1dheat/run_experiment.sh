#!/usr/bin/env bash

#SBATCH --job-name=error_data
#SBATCH --output=error_data.log
#SBATCH --ntasks-per-node=14
#SBATCH --nodes=1
#SBATCH --time=10:00:00
#SBATCH -p long

# unload any modules currently loaded
module purge

# load the Fujitsu module
module load fujitsu/compiler/4.5 python/3.9.5

. ../heateq-mpark/mpark-env/bin/activate

#just error
python error_experiment.py
#energy
#/var/lib/pcp/pmdas/perfevent/perfalloc &
#python arm_energy_experiment.py
