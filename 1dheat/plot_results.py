import json
import pandas as pd 
import numpy as np 
import matplotlib.pyplot as plt 
import matplotlib.cm as cm
import argparse as ap 
import os 

def plot_test_list(test, size, save=False, save_type="png"):
	with open(f'./{test}/runs_{test}_{size}.json') as f:
		runs = json.load(f)

	if not os.path.exists(f'./images/{test}'):
		os.makedirs(f'./images/{test}')

	colors = cm.turbo(np.linspace(0, 1, len(runs)))
	for i, run in enumerate(runs):
		plt.loglog(run['dts'], run['errs'], label=f'{run["fname"]}', color=colors[i])
		plt.scatter(run['dts'], run['errs'], color=colors[i], s=(len(runs)-i)*20)

	plt.title(f'SSL2-{test.upper()}, Size: {size}')
	plt.xlabel('dt')
	plt.ylabel('error')
	#plt.ylim(top=1e-7)
	#plt.xticks(run['dts'])
	plt.grid()
	#plt.ylim(top=1e0)
	plt.legend()
	if not save:
		plt.show()
	else:
		plt.savefig(f'./images/{test}/err_v_dt_{size}.{save_type}', format=f'{save_type}', dpi=1000)
		plt.clf()

	##################

	for i, run in enumerate(runs):
		plt.loglog(run['dts'], run['ts'], label=f'{run["fname"]}', color=colors[i])
		plt.scatter(run['dts'], run['ts'], color=colors[i], s=(len(runs)-i)*20)

	plt.title(f'SSL2-{test.upper()}, Size: {size}')
	plt.xlabel('dt')
	plt.ylabel('Runtime (s)')
	#plt.ylim(top=1e-3)
	#plt.xticks(run['dts'])
	plt.grid()
	#plt.ylim(top=1e0)
	plt.legend()
	if not save:
		plt.show()
	else:
		plt.savefig(f'./images/{test}/ts_v_dt_{size}.{save_type}', format=f'{save_type}', dpi=1000)
		plt.clf()

	##################

	for i, run in enumerate(runs):
		plt.loglog(run['ts'], run['errs'], label=f'{run["fname"]}', color=colors[i])
		plt.scatter(run['ts'], run['errs'], color=colors[i], s=(len(runs)-i)*20)

	plt.title(f'SSL2-{test.upper()}, Size: {size}')
	plt.xlabel('Runtime (s)')
	plt.ylabel('Error')
	#plt.ylim(top=1e-7)
	#plt.xticks(run['dts'])
	plt.grid()
	#plt.ylim(top=1e0)
	plt.legend()
	if not save:
		plt.show()
	else:
		plt.savefig(f'./images/{test}/ts_v_err_{size}.{save_type}', format=f'{save_type}', dpi=1000)
		plt.clf()

	##################

def main():
	parser = ap.ArgumentParser(description='Plot the results of the tests!')
	parser.add_argument('test', metavar='T', type=str, help='Name of the test to plot (dir name)')
	parser.add_argument('size', metavar='S', type=str, help='Number of gridpoints used in the test run')
	parser.add_argument('--save', action='store_true', help='If set saves plots else display')
	args = parser.parse_args()
	plot_test_list(args.test, args.size, save=args.save)

if __name__ == '__main__':
	main()