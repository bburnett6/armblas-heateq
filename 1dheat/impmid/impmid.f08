!frt -SSL2 -o impmid.x impmid.f08
!https://www.cc.kyushu-u.ac.jp/scp/eng/system/ITO/02-1_fujitsu_compiler.html

#ifndef NX
#define NX 20
#endif
#ifndef DEBUG
#define DEBUG 0
#endif

module utility_m
contains

	subroutine linspace(from, to, array, endpoint)
	!thank you: https://stackoverflow.com/a/57211848
	!modified to add endpoint true/false support
	implicit none
    real(8), intent(in) :: from, to
    real(8), intent(out) :: array(:)
    logical, intent(in) :: endpoint
    real(8) :: range
    integer :: n, d, i

    n = size(array)
    if (endpoint) then 
    	d = -1
    else
    	d = 0
    end if 

    range = to - from

    if (n == 0) return

    if (n == 1) then
        array(1) = from
        return
    end if

    do i=1, n
        array(i) = from + range * (i - 1) / (n + d)
    end do
	end subroutine linspace

	subroutine eye(n, matrix)
	implicit none 
	integer, value, intent(in) :: n 
	real(8), intent(out) :: matrix(n, n)
	integer :: i

	matrix = 0.0
	do i=1,n 
		matrix(i, i) = 1.0
	end do 

	end subroutine eye

	subroutine printmatrix(b,n,m)
	!http://math.hawaii.edu/~gautier/math_190_lecture_11.pdf
	integer::n,m
	real(8)::b(n,m) !n = # rows, m = # columns
	do i=1,n; print '(20f6.2)',b(i,1:m); enddo
	end subroutine printmatrix

end module utility_m

module impmid_m
contains	

	subroutine exact_solution(x, t, beta, uf)
	implicit none
	real(8), intent(in) :: x(NX), t, beta
	real(8), intent(out) :: uf(NX)
	real(8) :: pi = 4.0*atan(1.0) !supposedly this is pi: https://stackoverflow.com/questions/2157920/why-define-pi-4atan1-d0
	integer :: i 

	do i=1,NX
		uf(i) = exp(-4.0 * beta * t * pi) * sin(2.0 * pi * x(i))
	end do

	end subroutine exact_solution

	subroutine init_u(u, x)
	implicit none 
	real(8), intent(inout) :: u(NX)
	real(8), intent(in) :: x(NX)
	integer :: i
	real(8) :: pi = 4.0*atan(1.0) !supposedly this is pi: https://stackoverflow.com/questions/2157920/why-define-pi-4atan1-d0
	!real(8) :: pi = 3.14159
	u = 0.0
	do i=1,NX-1 
		u(i) = sin(2.0 * pi * x(i))
	end do 

	end subroutine init_u

	subroutine init_a(a, gamma)
	implicit none 
	real(8), value, intent(in) :: gamma
	real(8), intent(inout) :: a(NX, NX)
	integer :: i 

	a = 0.0
	do i=2,NX-1
		a(i, i+1) = gamma 
		a(i, i) = -2.0 * gamma 
		a(i, i-1) = gamma 
	end do
	a(1, 1) = 1.0
	a(NX, NX) = 1.0

	end subroutine init_a

	subroutine init_b(a, b)
	use utility_m
	implicit none
	real(8), intent(in) :: a(NX, NX)
	real(8), intent(inout) :: b(NX, NX)

	call eye(NX, b)
	b = b - a / 2.0

	end subroutine init_b
	
	function impmid_driver(nt, ts, xs, dx, init_dt, ti, tf, beta) result(u)
	!http://www.aertia.com/docs/lahey/ssl2_lin62.pdf
	implicit none 
	integer, value, intent(in) :: nt 
	real(8), intent(in) :: ts(:), xs(NX), dx, init_dt, ti, tf, beta
	real(8) :: t_f, dt, t, gamma, eps
	real(8) :: a(NX, NX), b(NX, NX), u(NX), y1(NX), tmp(NX), alu_det_info(NX), alu_workspace(NX)
	integer :: i, stat, pivots(NX)
	
	!eps = epsilon(dx) !machine precision to use for LU decomposition.
	eps = 0.0 !when eps is zero it uses a standard value
	!initializations
	dt = init_dt
	gamma = beta * dt / (dx * dx)
	call init_u(u, xs)
	
	!initialize the matricies for LU. Only perform one LU decompose on b 
	!then use the solvers on decomposed b each iteration.
	call init_a(a, gamma)
	call init_b(a, b)

	!http://www.aertia.com/docs/lahey/ssl2_lin62.pdf pg 98 for ALU
	call DALU(b, NX, NX, eps, pivots, alu_det_info, alu_workspace, stat)

	t_f = ti 
	!run time stepping loop
	do i=1,size(ts)
		t = ts(i)
		!implicit step
		y1 = u 
		!http://www.aertia.com/docs/lahey/ssl2_lin62.pdf pg 454 for LUX
		call DLUX(y1, b, NX, NX, 1, pivots, stat)
		!update step
		!http://www.aertia.com/docs/lahey/ssl2_lin62.pdf pg 456 for MAV
		call DMAV(a, NX, NX, -NX, -1.0 * y1, u, stat)
		!update total time
		t_f = t_f + dt 
	end do 

	if (DEBUG == 1) then
		print *, 'end time = ', t
		print *, 'final time = ', t_f
	end if

	end function impmid_driver

end module impmid_m 

program main
	use impmid_m
	use utility_m 
	implicit none 
	integer :: nt
	real(8) :: l2_err, tmp(NX), u_ref(NX), start, finish
	real(8) :: beta, ti, tf, xi, xf, dx, dt
	real(8) :: xs(NX), uf(NX)
	real(8), allocatable :: ts(:)
	character(len=32) :: arg

	beta = 1.0
	ti = 0.0
	tf = 0.1
	xi = 0.0
	xf = 1.0 

	call linspace(xi, xf, xs, .TRUE.) !spacial grid contains endpoint
	dx = xs(2) - xs(1)

	call getarg(1, arg)
	read(arg, *)nt 
	allocate(ts(nt))
	call linspace(ti, tf, ts, .FALSE.) !times does not contain endpoint
	dt = ts(2) - ts(1)
	if (DEBUG == 1) then 
		!print *, 'ts = ', ts(:)
		print *, 'dt = ', dt
	end if 

	call cpu_time(start)
	uf = impmid_driver(nt, ts, xs, dx, dt, ti, tf, beta)
	call cpu_time(finish)
	print *, "method duration: ", finish-start

	if (DEBUG == 1) then
	!	print *, "uf: ", uf(:)
	end if

	open(unit=2, file="../ref_sol/ref_sol.txt")
	read (2,*) u_ref(:) 
	close(2)

	tmp = uf - u_ref
	l2_err = sqrt(dx * dot_product(tmp, tmp))
	print *, "L2 Error with reference: ", l2_err

	deallocate(ts)

end program main
