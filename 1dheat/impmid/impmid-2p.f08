!frt -SSL2 -o impmid-ss.x impmid-ss.f08 -cpp -DNX=20
!https://www.cc.kyushu-u.ac.jp/scp/eng/system/ITO/02-1_fujitsu_compiler.html

#ifndef NX
#define NX 20
#endif
#ifndef DEBUG
#define DEBUG 0
#endif
#ifndef FULL_TYPE
#define FULL_TYPE real64
#endif
#ifndef REDU_TYPE
#define REDU_TYPE real32
#endif

module utility_m
contains

	subroutine linspace(from, to, array, endpoint)
	!thank you: https://stackoverflow.com/a/57211848
	!modified to add endpoint true/false support
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
    real(fp), intent(in) :: from, to
    real(fp), intent(out) :: array(:)
    logical, intent(in) :: endpoint
    real(fp) :: range
    integer :: n, d, i

    n = size(array)
    if (endpoint) then 
    	d = -1
    else
    	d = 0
    end if 

    range = to - from

    if (n == 0) return

    if (n == 1) then
        array(1) = from
        return
    end if

    do i=1, n
        array(i) = from + range * (i - 1) / (n + d)
    end do
	end subroutine linspace

	subroutine eye_fp(n, matrix)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none 
	integer, value, intent(in) :: n 
	real(fp), intent(out) :: matrix(n, n)
	integer :: i

	matrix = 0.0
	do i=1,n 
		matrix(i, i) = 1.0_fp
	end do 

	end subroutine eye_fp

	subroutine eye_rp(n, matrix)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none 
	integer, value, intent(in) :: n 
	real(rp), intent(out) :: matrix(n, n)
	integer :: i

	matrix = 0.0
	do i=1,n 
		matrix(i, i) = 1.0_rp
	end do 

	end subroutine eye_rp

	subroutine printmatrix(b,n,m)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	!http://math.hawaii.edu/~gautier/math_190_lecture_11.pdf
	integer::n,m
	real(fp)::b(n,m) !n = # rows, m = # columns
	do i=1,n; print '(20f6.2)',b(i,1:m); enddo
	end subroutine printmatrix

	subroutine printmatrix_rp(b,n,m)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	!http://math.hawaii.edu/~gautier/math_190_lecture_11.pdf
	integer::n,m
	real(rp)::b(n,m) !n = # rows, m = # columns
	do i=1,n; print '(20f6.3)',b(i,1:m); enddo !printing for half precision not supported so comment out
	end subroutine printmatrix_rp

end module utility_m

module impmid_m
contains	

	subroutine exact_solution(x, t, beta, uf)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	real(fp), intent(in) :: x(NX), t, beta
	real(fp), intent(out) :: uf(NX)
	real(fp) :: pi = 4.0*atan(1.0) !supposedly this is pi: https://stackoverflow.com/questions/2157920/why-define-pi-4atan1-d0
	integer :: i 

	do i=1,NX
		uf(i) = exp(-4.0_fp * beta * t * pi) * sin(2.0_fp * pi * x(i))
	end do

	end subroutine exact_solution

	subroutine init_u(u, x)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none 
	real(fp), intent(inout) :: u(NX)
	real(fp), intent(in) :: x(NX)
	integer :: i
	real(fp) :: pi = 4.0*atan(1.0) !supposedly this is pi: https://stackoverflow.com/questions/2157920/why-define-pi-4atan1-d0
	!real(fp) :: pi = 3.14159
	u = 0.0
	do i=1,NX-1 
		u(i) = sin(2.0_fp * pi * x(i))
	end do 

	end subroutine init_u

	function fi_f(u, t, dx, beta) result(fn)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none 
	real(fp), intent(in) :: u(NX), t, dx, beta 
	real(fp) :: fn(NX)
	integer :: i

	fn = 0.0

	do i=2,NX-1
		fn(i) = beta/(dx * dx) * (u(i-1) + u(i+1) - 2.0_fp * u(i)) 
	end do

	end function fi_f

	function fi_r(u, t, dx, beta) result(fn)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none 
	real(rp), intent(in) :: u(NX), t, dx, beta 
	real(rp) :: fn(NX)
	integer :: i

	fn = 0.0

	do i=2,NX-1
		fn(i) = beta/(dx * dx) * (u(i-1) + u(i+1) - 2.0_rp * u(i)) 
	end do

	end function fi_r

	subroutine init_a(a, gamma)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none 
	real(fp), value, intent(in) :: gamma
	real(fp), intent(inout) :: a(NX, NX)
	integer :: i 

	a = 0.0_rp
	do i=2,NX-1
		a(i, i+1) = gamma 
		a(i, i) = -2.0_rp * gamma 
		a(i, i-1) = gamma 
	end do
	a(1, 1) = 1.0_rp 
	a(NX, NX) = 1.0_rp 

	end subroutine init_a

	subroutine init_b(a, b)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	use utility_m
	implicit none
	real(rp), intent(in) :: a(NX, NX)
	real(rp), intent(inout) :: b(NX, NX)

	call eye_rp(NX, b)
	b = b - a / 2.0_rp

	end subroutine init_b
	
	function impmid_driver(nt, ts, xs, dx, init_dt, ti, tf, beta) result(u)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	!http://www.aertia.com/docs/lahey/ssl2_lin62.pdf
	implicit none 
	integer, value, intent(in) :: nt 
	real(fp), intent(in) :: ts(:), xs(NX), dx, init_dt, ti, tf, beta
	real(fp) :: t_f, dt, t, gamma, eps, a(NX, NX), u(NX), yk(NX), tmp(NX)
	real(rp) :: b(NX, NX), y1(NX), alu_det_info(NX), alu_workspace(NX)
	integer :: i, j, k, stat, pivots(NX), n_corr=2
	
	!eps = epsilon(dx) !machine precision to use for LU decomposition.
	eps = 0.0 !when eps is zero it uses a standard value
	!initializations
	dt = init_dt
	gamma = beta * dt / (dx * dx)
	call init_u(u, xs)
	
	!initialize the matricies for LU. Only perform one LU decompose on b 
	!then use the solvers on decomposed b each iteration.
	call init_a(a, gamma)
	call init_b(real(a, rp), b)

	!http://www.aertia.com/docs/lahey/ssl2_lin62.pdf pg 98 for ALU
	call ALU(b, NX, NX, eps, pivots, alu_det_info, alu_workspace, stat)

	t_f = ti 
	!run time stepping loop
	do i=1,size(ts)
		t = ts(i)
		!implicit step
		y1 = real(u, rp)
		!http://www.aertia.com/docs/lahey/ssl2_lin62.pdf pg 454 for LUX
		call LUX(y1, b, NX, NX, 1, pivots, stat)
		!correction step 
		yk = real(y1, fp)
		do j=1,n_corr
			!performing: yk = u + dt/2 * F(yk)
			!one DMAV would require the step to be yk = yk + dt/2 *F(yk)
			tmp(:) = u(:)
			call DMAV(a, NX, NX, -NX, -0.5_fp * yk, tmp, stat)
			yk(:) = tmp(:)
			!maybe this way?
			!do k=2,NX-1
			!	yk(k) = u(k) + dt*beta/(2.0_fp * dx * dx) * (yk(k-1) + yk(k+1) - 2.0_fp * yk(k)) 
			!end do
		end do 
		!update step
		!http://www.aertia.com/docs/lahey/ssl2_lin62.pdf pg 456 for MAV
		call DMAV(a, NX, NX, -NX, -1.0_fp * yk, u, stat)
		!update total time
		t_f = t_f + dt 
	end do 

	if (DEBUG == 1) then
		print *, 'end time = ', t
		print *, 'final time = ', t_f
	end if

	end function impmid_driver

end module impmid_m 

program main
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE, qp=>real128
	use impmid_m
	use utility_m 
	implicit none 
	integer :: nt
	real(fp) :: l2_err, tmp(NX), u_ref(NX), start, finish
	real(fp) :: beta, ti, tf, xi, xf, dx, dt
	real(fp) :: xs(NX), uf(NX)
	real(fp), allocatable :: ts(:)
	character(len=32) :: arg
	character(len=32) :: fname
	character(len=32) :: tmp_char

	beta = 1.0_fp 
	ti = 0.0_fp 
	tf = 0.1_fp 
	xi = 0.0_fp 
	xf = 1.0_fp 

	call linspace(xi, xf, xs, .TRUE.) !spacial grid contains endpoint
	dx = xs(2) - xs(1)

	call getarg(1, arg)
	read(arg, *)nt 
	allocate(ts(nt))
	call linspace(ti, tf, ts, .FALSE.) !times does not contain endpoint
	dt = ts(2) - ts(1)
	if (DEBUG == 1) then 
		!print *, 'ts = ', ts(:)
		print *, 'dt = ', dt
	end if 

	call cpu_time(start)
	uf = impmid_driver(nt, ts, xs, dx, dt, ti, tf, beta)
	call cpu_time(finish)
	print *, "method duration: ", finish-start

	if (DEBUG == 1) then
	!	print *, "uf: ", uf(:)
	end if

	write(tmp_char, "(I0.6)"), NX
	fname = "../ref_sol/ref_sol_"// trim(tmp_char) //".txt"
	open(unit=2, file=fname)
	read (2,*) u_ref(:) 
	close(2)

	tmp = uf - u_ref
	l2_err = sqrt(dx * dot_product(tmp, tmp))
	!l2_err = maxval(tmp)
	print *, "L2 Error with reference: ", l2_err

	deallocate(ts)

end program main